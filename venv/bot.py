import http.client
from enum import Enum
from datetime import datetime
import json
import dateutil.relativedelta
import requests
import vk_api
import strgen
from vk_api.longpoll import VkLongPoll, VkEventType

import random
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from vk_api.upload import VkUpload
import urllib.request
from dateutil.parser import parse

conn = http.client.HTTPSConnection("rawg-video-games-database.p.rapidapi.com")

headers = {
    'x-rapidapi-host': "rawg-video-games-database.p.rapidapi.com",
    'x-rapidapi-key': "token_key"
    }

chicken_conn = http.client.HTTPSConnection("chicken-coop.p.rapidapi.com")

chicken_headers = {
    'x-rapidapi-host': "chicken-coop.p.rapidapi.com",
    'x-rapidapi-key': "token_key"
    }

class Platform(Enum):
    ANDROID = 21
    IOS = 3
    PC = 4
    XBOX_360 = 14
    PS_4 = 18
    PS_3 = 16
    SWITCH = 7
    WRONG_PLATFORM = -1


class Mode(Enum):
    OTHER = 1
    INSERTING_YEAR = 2
    INSERTING_DEVELOPER = 3
    INSERTING_PLATFORM = 4
    INSERTING_FILTER = 5
    INSERTING_NAME = 0
class Filter(Enum):
    RATING = "-rating"
    POPULARITY = "-added"

def getGameInformation(gameName):
    try:
        queryStr = ("/games/{}").format(gameName).strip().replace(" ", "-")
        for ch in queryStr:

            if(ord(ch)>128):
                return ("No result")
        con=-1
        game=None

        queryStr= urllib.parse.quote(queryStr)


        chicken_conn.request("GET", queryStr , headers=chicken_headers)
        res = chicken_conn.getresponse()
        print(res.getcode())
        if(res.getcode()==200):
            con=res.getcode()
            data = res.read()
            game = json.loads(data.decode("utf-8"))["result"]
            return(game)
        else:
            return "No connection"
    except:
        return "No connection"
def   getGames(platform,developers,startTimeline, endTimeline, filter,number):
    try:
        platformStr=""
        queryStr="/games?"

        if startTimeline == None and endTimeline == None and platform==None and filter==None:
            queryStr = "/games"
        if startTimeline!=None and endTimeline!=None:
            queryStr += ("dates={0},{1}").format(startTimeline,endTimeline)
        else:
            queryStr+=""
        if platform!=None:
            if(platform[0]!=None):
                for p in platform:
                    if(p!=None):
                        platformStr+=str(p.value)+","
                platformStr = platformStr[:len(platformStr) - 1]

                if queryStr[:len(queryStr)-1]!="&" and queryStr[:len(queryStr)-1]!="?":
                    queryStr += "&"
                queryStr += ("platforms={}").format(platformStr)

        if developers!=None:

            if queryStr[:len(queryStr)-1]!="&" and queryStr[:len(queryStr)-1]!="?":
                queryStr+="&"
            queryStr += "developers=" + str(developers)

        if filter!=None:

            if queryStr[:len(queryStr)-1]!="&" and queryStr[:len(queryStr)-1]!="?":
                queryStr+="&"
            queryStr += "ordering=" + filter.value

        print(queryStr)
        # queryStr = urllib.parse.quote(queryStr)
        conn.request("GET",queryStr, headers=headers)
        res = conn.getresponse()
        data = res.read()

        print(data.decode("utf-8"))

        result = json.loads(data.decode("utf-8"))["results"]
        number=(len(result),number)[len(result)>=number];
        answerString=""
        if(number>0):
            for i in range(number):
                if(result[i]['rating']<3):
                    answerString+=str(i + 1) + ")" + str(result[i]["name"])+'\n'
                else:
                    answerString+=str(i+1)+")"+result[i]["name"]+": "+str(result[i]['rating'])+'\n'
        return answerString
    except:
        return "Извините, возникла ошибка при обращении к серверу"
def getLatestGames(platform,developers, filter):

    currentdate = datetime.today()
    startdate = currentdate - dateutil.relativedelta.relativedelta(months=2)

    currentdate = currentdate.strftime('%Y-%m-%d')
    startdate = startdate.strftime('%Y-%m-%d')

    getGames(platform,developers, startdate,currentdate,filter,number)

def getGamesByYear(platform,developers,year,filter,number):
    startDate=str(year)+"-01-01"
    endDate=str(year)+"-12-31"
    return getGames(platform,developers, startDate, endDate, filter,number)

def getDeveloperId(name):
    conn.request("GET", ("/developers?search={}&page_size=1").format(name).replace(" ","%20"), headers=headers)
    res = conn.getresponse()
    data = res.read()
    data.decode("utf-8")
    print(data)
    if res.getcode()==200:
        if json.loads(data)["results"][0]["id"]!=None and len(json.loads(data)["results"])==1 and json.loads(data)["results"][0]["id"]>0:
            print(json.loads(data)["results"][0])
            return json.loads(data)["results"][0]
        else:
            return None
    else:
        return None
def getPlatformMode(text):
    if(text=='ПК'):
        return Platform.PC
    elif(text=='Android'):
        return Platform.ANDROID
    elif(text=='IOS'):
        return Platform.IOS
    elif (text == 'XBOX 360'):
        return Platform.XBOX_360
    elif (text == 'PS3'):
        return Platform.PS_3
    elif (text == 'PS4'):
        return Platform.PS_4
    elif (text == 'Switch'):
        return Platform.SWITCH
    elif (text == 'All'):
        return None
    else:
        return Platform.WRONG_PLATFORM


# getGamesByYear([Platform.PC],getDeveloperId("Electronic Arts")["id"],2015,Filter.POPULARITY,5)

# vk-api --------------------------------------------------------

class User():
    id=""
    searchingPlatform = None
    searchingYear = -1
    searchingFilter = ""
    mode=Mode.OTHER
    def __init__(self, id):
        """Constructor"""
        self.id = id
userList =[]
def existsInList(id):
    for user in userList:

        if user.id==id:
            return True

    return False
def getUser(id):
    for user in userList:
        print(user.id)
        if user.id==id:
            return user
    return None




vk_session = vk_api.VkApi(token='token_key')
longpoll = VkLongPoll(vk_session)
vk = vk_session.get_api()

def get_random_id():
    """ Get random int32 number (signed) """
    return random.getrandbits(31) * random.choice([-1, 1])
def get_keyboard(self):
        """ Получить json клавиатуры """
        return sjson_dumps(self.keyboard)



for event in longpoll.listen():

    if event.type == VkEventType.MESSAGE_NEW and event.to_me and len(event.text)>=1:
       if event.from_user:  # Если написали в ЛС
            currentUser=None

            if(existsInList(str(event.user_id))):
                currentUser=getUser(str(event.user_id))

            else:
                userList.append(User(str(event.user_id)))
                currentUser=userList[-1]

            if (event.text == "Главное меню" or event.text == "Начать"):
                vk.messages.send(  # Отправляем сообщение
                    user_id=event.user_id,
                    message='Привет! Я бот, написанный Вадимом Филиным и выводящий игровую инфографику.'
                            + ' Выбери действие на клавиатуре, чтобы начать работу.',
                    keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                    random_id=get_random_id())
                currentUser.mode = Mode.OTHER
            elif (event.text == "Назад"):
                vk.messages.send(  # Отправляем сообщение
                    user_id=event.user_id,
                    message='Возвращаемся на главное меню.',
                    keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                    random_id=get_random_id())
                currentUser.mode = Mode.OTHER
            elif(event.text=="Топ популярных игр в этом году"):
                currentUser.mode=Mode.OTHER
                vk.messages.send(  # Отправляем сообщение
                    user_id=event.user_id,
                    message=getGamesByYear(None,None,str(datetime.now().year),Filter.POPULARITY,10),
                    keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                    random_id=get_random_id())
            elif (event.text == "Настраиваемый топ игр"):
                currentUser.mode = Mode.INSERTING_PLATFORM

                vk.messages.send(  # Отправляем сообщение
                    user_id=event.user_id,
                    message="Выбери игровую платформу",
                    keyboard=open("platform_filter.json", "r", encoding="UTF-8").read(),
                    random_id=get_random_id())
            elif (event.text == "Найти информацию об игре:" or currentUser.mode == Mode.INSERTING_NAME):

                if(event.text == "Найти информацию об игре:" ):
                    vk.messages.send(  # Отправляем сообщение
                        user_id=event.user_id,
                        message="Введите название игры:",
                        keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                        random_id=get_random_id())
                    currentUser.mode = Mode.INSERTING_NAME
                else:
                    text = getGameInformation(event.text)
                    if(text=="No result"):
                        vk.messages.send(  # Отправляем сообщение
                            user_id=event.user_id,
                            message="Извини, такая игра не найдена, или ты ввёл некорректные данные.",
                            keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                            random_id=get_random_id())
                    elif (text == "No connection"):
                            vk.messages.send(  # Отправляем сообщение
                                user_id=event.user_id,
                                message="Извини, произошла ошибка соединения с одним из API. Попробуй ещё раз.",
                                keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                                random_id=get_random_id())
                    else:
                        print(text)
                        genres=text["genre"]
                        genres=", ".join(genres)
                        try:
                            date= parse(text["releaseDate"].replace("," , ""))
                            text=text["title"]+"\n\nДата: "+date.strftime('%d/%m/%Y')+"\n\nОписание:\n"+text["description"]+"\n\nЖанры: "+ genres +\
                                 "\n\nРазработчик: "+text["developer"]
                        except:
                            date= str(text["releaseDate"])
                            text = text["title"] + "\n\nДата: " + date + "\n\nОписание:\n" + text[
                                "description"] + "\n\nЖанры: " + genres + \
                                "\n\nРазработчик: " + text["developer"]

                        vk.messages.send(  # Отправляем сообщение
                            user_id=event.user_id,
                            message=text,
                            keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                            random_id=get_random_id())
                        currentUser.mode = Mode.OTHER
            # elif(event.text==""):

            elif (currentUser.mode == Mode.INSERTING_PLATFORM and event.text=="Назад"):
                currentUser.mode = Mode.OTHER
                vk.messages.send(  # Отправляем сообщение
                    user_id=event.user_id,
                    message="Возвращаемся на главное меню",
                    keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                    random_id=get_random_id())
            elif (currentUser.mode == Mode.INSERTING_PLATFORM):
                platform=getPlatformMode(event.text)
                if(getPlatformMode(event.text)!=Platform.WRONG_PLATFORM):
                    currentUser.searchingPlatform= platform
                    currentUser.mode= Mode.INSERTING_YEAR
                    vk.messages.send(  # Отправляем сообщение
                        user_id=event.user_id,
                        message="Напиши выбираемый год поиска",
                        keyboard=open("cancel_keyboard.json", "r", encoding="UTF-8").read(),
                        random_id=get_random_id())
                else:
                    vk.messages.send(  # Отправляем сообщение
                        user_id=event.user_id,
                        message="Прости, я не знаю такой платформы",
                        keyboard=open("platform_filter.json", "r", encoding="UTF-8").read(),
                        random_id=get_random_id())

            elif (currentUser.mode == Mode.INSERTING_FILTER):
                if(event.text=="По рейтингу"):
                    text = getGamesByYear([currentUser.searchingPlatform],None,currentUser.searchingYear,Filter.RATING,10)
                    vk.messages.send(  # Отправляем сообщение
                    user_id=event.user_id,
                    message=text,
                    keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                    random_id=get_random_id())
                    currentUser.mode = Mode.OTHER
                elif(event.text=="По популярности"):
                    text = getGamesByYear([currentUser.searchingPlatform], None, currentUser.searchingYear, Filter.POPULARITY, 10)
                    vk.messages.send(  # Отправляем сообщение
                        user_id=event.user_id,
                        message=text,
                        keyboard=open("keyboard.json", "r", encoding="UTF-8").read(),
                        random_id=get_random_id())
                    currentUser.mode=Mode.OTHER
                else:
                    vk.messages.send(  # Отправляем сообщение
                        user_id=event.user_id,
                        message="Извини, я тебя не понимаю, попробуй ещё раз :<",
                        keyboard=open("rating_keyboard.json", "r", encoding="UTF-8").read(),
                        random_id=get_random_id())
            elif (currentUser.mode == Mode.INSERTING_YEAR):
                try:
                    if( int( event.text )>=1970 and int( event.text )<=datetime.now().year):
                        n=2
                        print(currentUser.searchingPlatform)
                        if(currentUser.searchingPlatform!=None):
                            if(currentUser.searchingPlatform==platform.PS_4 and int(event.text)<2013):
                                n += "error"
                            if (currentUser.searchingPlatform==platform.PS_3 and int(event.text)<2006):
                                n += "error"
                            if  (currentUser.searchingPlatform == platform.SWITCH and int(event.text) < 2017):
                                n += "error"
                            if (currentUser.searchingPlatform == platform.IOS and int(event.text) < 2007):
                                n+="error"
                            if (currentUser.searchingPlatform == platform.ANDROID and int(event.text) < 2007):
                                n += "error"
                            if (currentUser.searchingPlatform == platform.XBOX_360 and int(event.text) < 2005):
                                n += "error"

                        currentUser.mode=Mode.INSERTING_FILTER
                        currentUser.searchingYear= int( event.text )

                        vk.messages.send(  # Отправляем сообщение
                            user_id=event.user_id,
                            message="По какому параметру отсортировать игры?",
                            keyboard=open("rating_keyboard.json", "r", encoding="UTF-8").read(),
                            random_id=get_random_id())
                    else:
                        vk.messages.send(  # Отправляем сообщение
                            user_id=event.user_id,
                            message="Напиши адекватный год для поиска.",
                            keyboard=open("cancel_keyboard.json", "r", encoding="UTF-8").read(),
                            random_id=get_random_id())



                except Exception as inst:
                    print(type(inst))  # the exception instance
                    print(inst.args)  # arguments stored in .args
                    print(inst)
                    vk.messages.send(  # Отправляем сообщение
                        user_id=event.user_id,
                        message="Напиши адекватный год для поиска.",
                        keyboard=open("cancel_keyboard.json", "r", encoding="UTF-8").read(),
                        random_id=get_random_id())


       print(currentUser.id+" "+str(currentUser.mode)+" "+event.text)
